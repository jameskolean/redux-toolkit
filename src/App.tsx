// src/App.tsx
import { useState } from "react";
import { useAppDispatch, useAppSelector } from "./app/hooks";
import {
  increment,
  decrement,
  incrementByAmount,
} from "./features/counter/counterSlice";
import { useFetchFactsQuery } from "./features/cats/catsApiSlice";
import "./App.css";

function App() {
  const counterValue = useAppSelector((state) => state.counter.value);
  const dispatch = useAppDispatch();
  const [incrementAmount, setIncrementAmount] = useState("2");
  const [factLimit, setfactLimit] = useState(5);
  const { data, isFetching = [] } = useFetchFactsQuery(factLimit);

  return (
    <div className="App">
      <h1>Vite + React + Redux Toolkit</h1>
      <div className="card">
        <div className="button-bar">
          <button onClick={() => dispatch(increment())}>+</button>
          <button onClick={() => dispatch(decrement())}>-</button>
        </div>
        <p> count is {counterValue}</p>
        <div className="button-bar">
          <input
            className="textbox"
            aria-label="Set increment amount"
            value={incrementAmount}
            onChange={(e) => setIncrementAmount(e.target.value)}
          />
          <button
            onClick={() =>
              dispatch(incrementByAmount(Number(incrementAmount) || 0))
            }
          >
            Add Amount
          </button>
        </div>
        <div>
          <p>Cat Facts</p>
          <ul>
            {data != null &&
              data.data.map(({ fact }, index) => (
                <li key={index}>{JSON.stringify(fact)}</li>
              ))}
          </ul>
        </div>
        <div className="button-bar">
          <p>Facts to Fetch</p>
          <input
            className="textbox"
            aria-label="Set fact limit"
            value={factLimit}
            onChange={(e) => setfactLimit(Number(e.target.value))}
          />
        </div>
      </div>
    </div>
  );
}

export default App;
