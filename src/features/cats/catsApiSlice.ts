// src/features/cats/catsApiSlice.ts
import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const apiSlice = createApi({
  reducerPath: "api",
  baseQuery: fetchBaseQuery({
    baseUrl: "https://catfact.ninja",
    prepareHeaders(headers) {
      // set additional headers hers
      return headers;
    },
  }),
  endpoints(builder) {
    return {
      fetchFacts: builder.query<CatFactPage, number | void>({
        query(limit = 5) {
          return `/facts?limit=${limit}`;
        },
      }),
    };
  },
});

export const { useFetchFactsQuery } = apiSlice;

interface CatFactPage {
  current_page: number;
  data: [
    {
      fact: string;
      length: number;
    }
  ];
  first_page_url: string;
  from: number;
  last_page: number;
  last_page_url: string;
  links: [
    {
      url: string | null;
      label: string;
      active: boolean;
    }
  ];
  next_page_url: string;
  path: string;
  per_page: number;
  prev_page_url: string | null;
  to: number;
  total: number;
}
